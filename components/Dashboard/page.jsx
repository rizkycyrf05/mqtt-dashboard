"use client";

import React, { useState } from "react";
import DeviceList from "./deviceList";
import client from "@/services/mqttConnection";
import devicesData from "./devices.json";

const Dashboard = () => {
  const [devices, setDevices] = useState(devicesData);

  const handleControlDevice = async (deviceId, message) => {
    try {
      console.log(`Mengirim pesan ke perangkat ${deviceId}: ${message}`);

      const device = devices.find((d) => d.id === deviceId);
      const mqttTopic = device ? device.mqtt_topic : "";

      if (mqttTopic) {
        client.publish(mqttTopic, message, (err) => {
          if (err) {
            console.error(
              `Gagal mengirim pesan MQTT ke ${mqttTopic}: ${message}`
            );
          } else {
            console.log(
              `Pesan MQTT berhasil dikirim ke ${mqttTopic}: ${message}`
            );
          }
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      {/* <h1 className="text-2xl font-bold mb-4 mt-6 text-center">Home</h1> */}
      <DeviceList devices={devices} handleControlDevice={handleControlDevice} />
    </div>
  );
};

export default Dashboard;
