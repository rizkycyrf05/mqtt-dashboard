import React from "react";

const DeviceList = ({ devices, handleControlDevice }) => {
  return (
    <ul className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 ml-6">
      {devices.map((device) => (
        <li key={device.id} className="bg-white rounded-lg shadow-lg p-6">
          <h3 className="text-lg font-semibold">{device.name}</h3>
          <p>Type: {device.type}</p>
          <p>Status: {device.status}</p>
          <div className="mt-2">
            {device.mqtt_messages.map((message) => (
              <button
                key={message.name}
                onClick={() => handleControlDevice(device.id, message.message)}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2 mb-2"
              >
                {message.name}
              </button>
            ))}
          </div>
        </li>
      ))}
    </ul>
  );
};

export default DeviceList;
