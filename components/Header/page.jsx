export default function Header() {
  return (
    <div className="bg-blue-600">
      <h1 className="text-xl font-semibold text-white p-4 italic">
        MQTT Dashboard
      </h1>
    </div>
  );
}
